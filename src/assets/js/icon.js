import { library } from '@fortawesome/fontawesome-svg-core'
import { fab } from '@fortawesome/free-brands-svg-icons'
import {
    faAngleDown, faCalendarAlt, faChevronDown,
    faEnvelope,
    faExternalLinkAlt, faInfoCircle,
    faMapMarkerAlt,
    faMobileAlt, faSadTear, faTimes, faTools, faUsers
} from '@fortawesome/free-solid-svg-icons'

library.add(
    fab,
    faAngleDown,
    faCalendarAlt,
    faChevronDown,
    faEnvelope,
    faExternalLinkAlt,
    faInfoCircle,
    faMobileAlt,
    faMapMarkerAlt,
    faSadTear,
    faTimes,
    faTools,
    faUsers,
);
