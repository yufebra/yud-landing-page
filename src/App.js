import React from "react";
import TopContentWelcome from "./components/SubPages/TopContentWelcome";
import NavBar from "./components/NavBar/NavBar";
import AboutMe from "./components/SubPages/AboutMe";
import Experiences from "./components/SubPages/Experiences";
import Footer from "./components/SubPages/Footer";
import MyContact from "./components/SubPages/MyContact";

require('./assets/js/icon');

class App extends React.Component{
    render() {
        return (
            <div className="App">
                <TopContentWelcome />
                <NavBar />
                <AboutMe />
                <Experiences />
                <MyContact />
                <Footer />
            </div>
        );
    }
}

export default App;
