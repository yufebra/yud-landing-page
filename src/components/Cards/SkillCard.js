import React from "react";
import BasicCard from "./BasicCard";
import FiveLevel from "../Others/FiveLevel";

class SkillCard extends React.Component{
    detectType(lv){
        switch (lv) {
            case 1:
                return 'Beginner';
            case 2:
                return 'Amateur';
            case 3:
                return 'Normal';
            case 4:
                return 'Confident';
            case 5:
                return 'Professional';
            default:
                return '-';
        }
    }

    render() {
        return(
            <BasicCard>
                <h1 className="font-extrabold text-lg">
                    <div className="stretch-row items-center">
                        <p className="w-full">{this.props.name}</p>
                        <div className="w-full">
                            <FiveLevel level={this.props.level} />
                        </div>
                    </div>
                </h1>
                <div className="stretch-row text-xs font-normal text-right">
                    <p>Since {this.props.startDate}</p>
                    <p>{this.detectType(this.props.level)}</p>
                </div>
            </BasicCard>
        )
    }
}

export default SkillCard;
