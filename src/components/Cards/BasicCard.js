import React from "react";
import "../../assets/css/index.css";

class BasicCard extends React.Component{
    render() {
        return (
            <div className="rounded-lg bg-white shadow-lg p-2">
                {this.props.children}
            </div>
        )
    }
}
export default BasicCard;
