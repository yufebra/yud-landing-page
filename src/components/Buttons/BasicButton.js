import React from "react";

class BasicButton extends React.Component{
    constructor(props) {
        super(props);
        this.state = {
            color: this.props.color ? this.selectColor(this.props.color) : "bg-blue-500 text-white",
        };
    }


    selectColor(color){
        switch (color) {
            case 'green':
                return "bg-green-600 text-white"
            default:
                return color;
        }
    }

    render() {
        return(
            <button className={`btn ${this.state.color}`} onClick={this.props.onClick}>
                {this.props.children}
            </button>
        )
    }
}

export default BasicButton;
