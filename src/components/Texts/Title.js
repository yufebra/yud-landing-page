import React from "react";

class Title extends React.Component{
    constructor(props) {
        super(props);
        let color = '';
        if (this.props.color === 'white') color = 'text-white border-white';
        else if (this.props.color === 'black') color = 'text-black border-black';
        else color = this.props.color;
        this.state = {
            colorClass: color,
        }
    }
    render() {
        return (
            <div className={`border-l-8 px-2 py-0.5 text-4xl font-extrabold mb-4 -ml-4 ${this.state.colorClass}`}>
                { this.props.children }
            </div>
        )
    }
}

Title.defaultProps = {
    color: 'black'
};

export default Title;
