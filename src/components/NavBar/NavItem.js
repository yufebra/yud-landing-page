import React from "react";

class NavItem extends React.Component {
    render() {
        return <a href={this.props.to}>{this.props.children}</a>;
    }
}

export default NavItem;
