import React from "react";
import NavItem from "./NavItem";

class NavBar extends React.Component {
    render() {
        return (
            <nav>
                <div className="w-full max-w-7xl flex justify-center">
                    <div className="flex gap-4">
                        <NavItem to="#about">About Me</NavItem>
                        <NavItem to="#experiences">Experiences</NavItem>
                        <NavItem to="#contact">Contact</NavItem>
                    </div>
                </div>
            </nav>
        )
    }
}

export default NavBar;
