import React from "react";

class FiveLevel extends React.Component{
    levelLoop(count, type) {
        let target = [];
        for (let i=0; i<count; i++) {
            target.push(<div key={i} className={`w-full ${type === 1 ? 'bg-green-500' : 'bg-gray-300'} h-3 rounded-full`} />);
        }
        return target;
    }

    render() {
        const level = this.props.level;
        if (0 <= level && level <= 5) {
            return (
                <div className="w-full flex gap-1">
                    {this.levelLoop(level, 1)}{this.levelLoop(5-level, 0)}
                </div>
            )
        }
    }
}

export default FiveLevel;
