import React from "react";
import {FontAwesomeIcon} from "@fortawesome/react-fontawesome";
import Title from "../Texts/Title";
import {Dialog, Transition, Disclosure} from "@headlessui/react";

class Experiences extends React.Component {
    constructor() {
        super();
        this.state = {
            selectedLink: {
                title: '',
                activeDate: '',
                description: '',
                link: '',
                tools: '',
                teamSize: '',
            },
            modalControl: false,
        }
    }

    clickLink(clicked) {
        // alert(clicked);
        this.setState({
            selectedLink: clicked,
            modalControl: true,
        });
    }

    controlModal = () => {
        this.setState({
            modalControl: !this.state.modalControl,
        })
    }

    render() {
        let iframeOrDiv = this.state.selectedLink.link.includes('http') ? (
            <iframe src={this.state.selectedLink.link} title={this.state.selectedLink.title}
                    className="h-semi w-full bg-white bg-gray-200"/>) : (
            <div className="h-semi flex flex-col gap-2 items-center justify-center text-gray-600 font-bold bg-gray-300">
                <FontAwesomeIcon icon="sad-tear" size="4x"/><p>Sorry, this page is not available</p></div>)
        const links = [
            {
                title: "Online Integrated Information System Across Members of SUCCESS",
                activeDate: "Nov 2020 - Now",
                description: "A management information system of SUCCESS that applies the values of efficiency, transparency, and a one-place information center that contains all data about this organization from the beginning until now and future, and some useful things like financial transparency, member list, and many more to facilitate management and it's members.",
                link: "https://oiisam.success.bateh.web.id",
                tools: 'Laravel, Vue.Js, Inertia.Js, Tailwind CSS, HeadlessUI',
                teamSize: '1 Full Stack Developer',
            },
            {
                title: "SUCCESS Festival 2021",
                activeDate: "June 2021 - August 2021",
                description: "A registration portal of SUCCESS Festival 2021 Event",
                link: "https://festival.success.bateh.web.id",
                tools: 'Laravel, Vue.Js, Inertia.Js, Tailwind CSS',
                teamSize: '1 Full Stack Developer, 5 Graphic Designer',
            },
            {
                title: "Debote",
                activeDate: "February 2021 - July 2021",
                description: "A web-based system that built using Django and Vue.js for simulating text-based debate against Artificial Intelligence for training purpose.\n" +
                    "This project is for my Final Project as student in D3 Teknik Informatika Politeknik Elektronika Negeri Surabaya",
                link: "",
                tools: 'Django, Vue.Js, Tailwind CSS',
                teamSize: '1 Full Stack Developer',
            },
            {
                title: "COBALT (Computer Based Test Simulation System)",
                activeDate: "June 2021 - August 2021",
                description: "A time limited multiple-choice test platform featuring multiple test package in one test and analytics for every participants and every questions.",
                link: "",
                tools: 'Laravel, Vue.Js, Inertia.Js, Tailwind CSS',
                teamSize: '1 Full Stack Developer',
            },
            {
                title: "Welcome Party Surabaya Cepu College Student Association 2020",
                activeDate: "July 2020 - September 2020",
                description: "A website for event registration + absent using QR code, randomizer, and a trivia game.",
                link: "",
                tools: 'Laravel, Vue.js, Bootstrap',
                teamSize: '1 Full Stack Developer',
            },
            {
                title: "My Health Diary",
                activeDate: "Mar 2020 - May 2020",
                description: "An android app built using Flutter (but Front-End only) for reminder and diary for doing healthy activities, your calories, and track your food nutrition.\n" +
                    "This project is for fulfill my college task in \"Pemrograman Lanjut\" course.",
                link: "",
                tools: 'Flutter',
                teamSize: '4 Mobile Developer',
            },
            {
                title: "COVID-19 World Stats using API from RapidAPI",
                activeDate: "Mar 2020 - April 2020",
                description: "A final project of Dicoding Fundamental Front-end Development Course",
                link: "",
                tools: 'Node.js, RapidAPI',
                teamSize: '1 Front-End Developer',
            },
        ];

        let linkCards = [];
        links.forEach((item, iter) => {
            linkCards.push(
                (
                    <div key={iter}>
                        <Disclosure>
                            {({open}) => (
                                <div>
                                    <Disclosure.Button
                                        className="flex items-center justify-between w-full px-4 py-2 text-sm text-left text-purple-900 bg-purple-200 rounded-lg hover:bg-purple-200 focus:outline-none focus-visible:ring focus-visible:ring-purple-500 focus-visible:ring-opacity-75">
                                        <div>
                                            <h3 className="font-bold">{item.title}</h3>
                                            <p className="text-xs">{item.activeDate}</p>
                                        </div>
                                        <FontAwesomeIcon
                                            icon="chevron-down"
                                            className={`${
                                                open ? 'transform rotate-180' : ''
                                            } w-5 h-5 text-purple-500`}
                                        />
                                    </Disclosure.Button>
                                    <Transition
                                        enter="transition duration-300 ease-out"
                                        enterFrom="transform -translate-y-full opacity-0"
                                        enterTo="transform translate-y-0 opacity-100"
                                        leave="transition duration-300 ease-out"
                                        leaveFrom="transform translate-y-0 opacity-100"
                                        leaveTo="transform -translate-y-full opacity-0"
                                    >
                                        <Disclosure.Panel
                                            className="px-4 py-2 mx-2 text-sm bg-purple-100 rounded-b-lg">
                                            <p className="mb-2">{item.description}</p>
                                            <button className="bg-blue-500 p-2 rounded-lg text-white px-2 py-1"
                                                    onClick={() => this.clickLink(item)}>More Detail and Preview
                                            </button>
                                        </Disclosure.Panel>
                                    </Transition>
                                </div>
                            )}
                        </Disclosure>
                    </div>
                )
            )
        });

        let modal = (
            <Transition appear show={this.state.modalControl}>
                <Dialog
                    as="div"
                    className="fixed inset-0 z-10 overflow-y-auto"
                    onClose={this.controlModal}
                >
                    <div className="min-h-screen py-4 text-center">
                        <Transition.Child
                            enter="ease-out duration-300"
                            enterFrom="opacity-0"
                            enterTo="opacity-100"
                            leave="ease-in duration-200"
                            leaveFrom="opacity-100"
                            leaveTo="opacity-0"
                        >
                            <Dialog.Overlay className="fixed bg-black bg-opacity-70 inset-0"/>
                        </Transition.Child>
                        <div className=" grid grid-cols-1 md:grid-cols-3 gap-4">
                            {/*Left Side*/}
                            <div>
                                <Transition.Child
                                    enter="ease-out transform duration-500"
                                    enterFrom="-translate-x-full"
                                    enterTo="translate-x-0"
                                    leave="ease-in transform duration-500"
                                    leaveFrom="translate-x-0"
                                    leaveTo="-translate-x-full"
                                >
                                    <div
                                        className="text-left align-middle transition-all transform bg-white shadow-xl rounded-r-2xl">
                                        <Dialog.Title
                                            as="div"
                                            className="text-lg font-medium leading-6 text-gray-900 px-3 py-1 my-2 mr-2 border-l-8 border-green-600"
                                        >
                                            <h3 className="text-lg font-extrabold">{this.state.selectedLink.title}</h3>
                                            <p className="text-xs">{this.state.selectedLink.link}</p>
                                        </Dialog.Title>
                                        <div className="px-5 py-1 text-sm">
                                            <table className="w-full divide-y divide-gray-400">
                                                <tr>
                                                    <td><FontAwesomeIcon icon="calendar-alt" fixedWidth/></td>
                                                    <td>{this.state.selectedLink.activeDate}</td>
                                                </tr>
                                                <tr>
                                                    <td><FontAwesomeIcon icon="tools" fixedWidth/></td>
                                                    <td>{this.state.selectedLink.tools}</td>
                                                </tr>
                                                <tr>
                                                    <td><FontAwesomeIcon icon="users" fixedWidth/></td>
                                                    <td>{this.state.selectedLink.teamSize}</td>
                                                </tr>
                                                <tr>
                                                    <td><FontAwesomeIcon icon="info-circle" fixedWidth/></td>
                                                    <td>{this.state.selectedLink.description}</td>
                                                </tr>
                                            </table>
                                        </div>
                                        <button
                                            className="absolute top-0 right-0 -mt-2 -mr-2 h-8 w-8 bg-red-600 hover:bg-red-400 text-white rounded-full text-center p-1"
                                            onClick={this.controlModal}>
                                            <FontAwesomeIcon icon="times"/>
                                        </button>
                                    </div>
                                </Transition.Child>
                            </div>
                            {/*Right Side*/}
                            <div className="md:col-span-2">
                                <Transition.Child
                                    enter="ease-out transform duration-500"
                                    enterFrom="translate-x-full"
                                    enterTo="translate-x-0"
                                    leave="ease-in transform duration-500"
                                    leaveFrom="translate-x-0"
                                    leaveTo="translate-x-full"
                                >
                                    <div
                                        className="overflow-hidden text-left transition-all transform bg-white shadow-xl lg:rounded-l-2xl">
                                        <div className="bg-green-600 text-white stretch-row items-center">
                                            <div className="p-2 overflow-hidden font-extrabold">
                                                <p>Browser View</p>
                                            </div>
                                            <a href={this.state.selectedLink.link} target="_blank" rel="noreferrer"
                                               className="bg-blue-600 p-2 rounded-l-lg">
                                                <FontAwesomeIcon icon="external-link-alt" size="sm" className="mr-1"/>Open
                                                in New Tab
                                            </a>
                                        </div>
                                        {iframeOrDiv}
                                    </div>
                                </Transition.Child>
                            </div>
                        </div>
                    </div>
                </Dialog>
            </Transition>
        );

        return (
            <div id="experiences" className="center-screen bg-blue-300">
                {modal}
                <div className="w-full max-w-5xl p-2">
                    <Title>Experiences and Portfolio</Title>
                    <p className="italic text-sm mb-2">Click title to expand description, then click the button inside drawer to open preview and more detailed development information.</p>
                    <div className="bg-white rounded-xl p-2 grid grid-cols-1 gap-2">
                        {linkCards}
                    </div>
                </div>
            </div>
        )
    }
}

export default Experiences;
