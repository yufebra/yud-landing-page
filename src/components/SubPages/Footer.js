import React from "react";

class Footer extends React.Component {
    render() {
        return (
            <div className="flex justify-center">
                <div className="w-full max-w-5xl p-2 stretch-row items-center">
                    <p>&copy; 2021, Yudha Febri Rastuama</p>
                    <div>
                        <div className="flex gap-1 items-center">
                            <p>This page was built using:</p>
                            <a href="https://reactjs.org" target="_blank" rel="noreferrer nofollow">
                                <img alt="react" className="h-8"
                                     src="https://upload.wikimedia.org/wikipedia/commons/a/a7/React-icon.svg"/>
                            </a>
                            <a href="https://tailwindcss.com" target="_blank" rel="noreferrer nofollow">
                                <img alt="tailwind" className="h-8 py-1"
                                     src="https://tailwindcss.com/_next/static/media/tailwindcss-mark.cb8046c163f77190406dfbf4dec89848.svg"/>
                            </a>
                            <a href="https://headlessui.dev" target="_blank" rel="noreferrer nofollow">
                                <img alt="headlessui" className="h-8"
                                     src="https://headlessui.dev/apple-touch-icon.png"/>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}

export default Footer;
