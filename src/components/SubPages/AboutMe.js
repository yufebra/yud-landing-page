import React from "react";
import {FontAwesomeIcon} from "@fortawesome/react-fontawesome";
import Title from "../Texts/Title";
import {Tab} from '@headlessui/react'
import EducationBackground from "../PagesComponent/EducationBackground";
import SkillCard from "../Cards/SkillCard";
import BasicCard from "../Cards/BasicCard";

class AboutMe extends React.Component {
    classNames(...classes) {
        return classes.filter(Boolean).join(' ')
    }

    render() {
        let tabs = {
            "College Educations": (<EducationBackground />),
            "Work History": (
                <div className="grid-c1 gap-2">
                    <BasicCard>
                        <h1 className="font-extrabold text-lg">PT. Pegadaian</h1>
                        <p className="text-xs">Internship | July 2020 - September 2020</p>
                    </BasicCard>
                </div>
            ),
            "Organization History": (
                <div className="grid-c1 gap-2">
                    <BasicCard>
                        <h1 className="font-extrabold text-lg">Himpunan Mahasiswa Teknik Informatika PENS</h1>
                        <p className="text-xs">Staff Muda Pengembangan Sumber Daya Mahasiswa | 2019</p>
                    </BasicCard>
                    <BasicCard>
                        <h1 className="font-extrabold text-lg">Himpunan Mahasiswa Teknik Informatika PENS</h1>
                        <p className="text-xs">Staff Ahli Pengembangan Sumber Daya Mahasiswa | 2020</p>
                    </BasicCard>
                    <BasicCard>
                        <h1 className="font-extrabold text-lg">Surabaya-Cepu College Student Association</h1>
                        <p className="text-xs">Kepala Divisi Pengembangan Sumber Daya Anggota | 2020</p>
                    </BasicCard>
                </div>
            ),
            "Achievements": (
                <div className="grid-c1 gap-2">
                    <BasicCard>
                        <h1 className="font-extrabold text-lg">Final Project Competition PENS 2021</h1>
                        <p className="text-xs">2nd Place of D3 Teknik Informatika Category | Individual | August 2021</p>
                    </BasicCard>
                    <BasicCard>
                        <h1 className="font-extrabold text-lg">Software Expo PENS 2020</h1>
                        <p className="text-xs">1st Place of D3 Teknik Informatika Category | Team | January 2020</p>
                    </BasicCard>
                </div>
            ),

        }
        let skills = {
            "Frameworks": (
                <div className="grid-c1 gap-2">
                    <SkillCard level={4} name={"Laravel"} startDate="August 2019" />
                    <SkillCard level={3} name={"Vue.js"} startDate="September 2020" />
                    <SkillCard level={3} name={"Tailwindcss"} startDate="September 2020" />
                    <SkillCard level={3} name={"Bootstrap"} startDate="January 2019" />
                    <SkillCard level={2} name={"Django"} startDate="February 2021" />
                    <SkillCard level={1} name={"React.js"} startDate="August 2021" />
                </div>
            ),
            "Programming Language": (
                <div className="grid-c1 gap-2">
                    <SkillCard level={4} name={"PHP"} startDate="March 2019"/>
                    <SkillCard level={3} name={"HTML/CSS"} startDate="August 2018" />
                    <SkillCard level={3} name={"JavaScript"} startDate="March 2019"/>
                    <SkillCard level={2} name={"Python"} startDate="February 2020"/>
                    <SkillCard level={2} name={"C++"} startDate="January 2020" />
                    <SkillCard level={1} name={"Bash"} startDate="January 2019" />
                </div>
            ),
            "Tools": (
                <div className="grid-c1 gap-2">
                    <SkillCard level={3} name={"Git"} startDate="August 2019"/>
                </div>
            ),
            "Major": (
                <div className="grid-c1 gap-2">
                    <SkillCard level={4} name={"Back-End Developer"} startDate="August 2019"/>
                    <SkillCard level={3} name={"Front-End Website Developer"} startDate="January 2020"/>
                    <SkillCard level={3} name={"UI/UX Designer"} startDate="August 2019"/>
                    <SkillCard level={2} name={"Android Developer"} startDate="July 2019"/>
                </div>
            ),
        }
        return (
            <div className="center-screen bg-blue-300" id="about">
                <div className="w-full max-w-5xl p-2">
                    <Title>About Me</Title>
                    <p className="mb-2">
                        My friends call me Yudha, I live in Cepu, Blora, Central Java, Indonesia. I'm an INFP person,
                        Cat Lover, and Linux Lover. My friends said that I am a creative dreamer with unique ideas. I
                        will always try to not let this dream become a mere dream.
                    </p>
                    <p className="mb-2">
                        I really enjoy the app development process as individual developer or as team, I always dare to
                        try even "it's impossible for me". Apart from being a Web Developer, I like Competitive
                        Programming too.
                    </p>
                    <p className="mb-10">
                        My motivation is to always try to make the environment around me better with the abilities I
                        have (in tech). Because of that, I maybe look like a "geeks" because I want to cover all that
                        environment needs.
                    </p>

                    <Title>My Paths to Reach Today</Title>
                    <div className="grid grid-cols-1 lg:grid-cols-2 gap-2">
                        <div>
                            <h2 className="text-center mb-2">My Experiences</h2>
                            <Tab.Group>
                                <Tab.List className="flex mx-2 rounded-xl">
                                    {Object.keys(tabs).map((category) => {
                                        return (
                                            <Tab
                                                key={category}
                                                className={({ selected }) =>
                                                    this.classNames(
                                                        'w-full py-2.5 text-sm leading-5 font-medium text-blue-700 rounded-t-lg',
                                                        'focus:outline-none focus:ring-2 ring-white ring-opacity-60',
                                                        selected
                                                            ? 'bg-gray-100'
                                                            : 'bg-gray-300 hover:bg-gray-500 hover:text-white'
                                                    )
                                                }
                                            >
                                                {category}
                                            </Tab>
                                        )
                                    })}
                                </Tab.List>
                                <Tab.Panels>
                                    {Object.values(tabs).map((posts, idx) => (
                                        <Tab.Panel
                                            key={idx}
                                            className={this.classNames(
                                                'bg-gray-200 rounded-lg p-3 max-h-80 overflow-y-auto',
                                                'focus:outline-none focus:ring-2 ring-offset-2 ring-offset-blue-400 ring-white ring-opacity-60'
                                            )}
                                        >
                                            {posts}
                                        </Tab.Panel>
                                    ))}
                                </Tab.Panels>
                            </Tab.Group>
                        </div>
                        <div>
                            <h2 className="text-center mb-2">My Skills and My Confidence Level</h2>
                            <Tab.Group>
                                <Tab.List className="flex mx-2 rounded-xl">
                                    {Object.keys(skills).map((category) => {
                                        return (
                                            <Tab
                                                key={category}
                                                className={({ selected }) =>
                                                    this.classNames(
                                                        'w-full py-2.5 text-sm leading-5 font-medium text-blue-700 rounded-t-lg',
                                                        'focus:outline-none focus:ring-2 ring-white ring-opacity-60',
                                                        selected
                                                            ? 'bg-gray-100'
                                                            : 'bg-gray-300 hover:bg-gray-500 hover:text-white'
                                                    )
                                                }
                                            >
                                                {category}
                                            </Tab>
                                        )
                                    })}
                                </Tab.List>
                                <Tab.Panels>
                                    {Object.values(skills).map((posts, idx) => (
                                        <Tab.Panel
                                            key={idx}
                                            className={this.classNames(
                                                'bg-gray-200 rounded-lg p-3 max-h-80 overflow-y-auto',
                                                'focus:outline-none focus:ring-2 ring-offset-2 ring-offset-blue-400 ring-white ring-opacity-60'
                                            )}
                                        >
                                            {posts}
                                        </Tab.Panel>
                                    ))}
                                </Tab.Panels>
                            </Tab.Group>
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}

export default AboutMe;
