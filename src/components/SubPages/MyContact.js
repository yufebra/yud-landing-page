import React from "react";
import {FontAwesomeIcon} from "@fortawesome/react-fontawesome";
import Title from "../Texts/Title";

class MyContact extends React.Component{
    render() {
        const contacts = [
            {
                icon: ['fab', 'github'],
                contact: 'yufebra',
                link: 'https://github.com/yufebra'
            },
            {
                icon: ['fab', 'gitlab'],
                contact: 'yufebra',
                link: 'https://gitlab.com/yufebra'
            },
            {
                icon: 'envelope',
                contact: 'yudhafebrirastuama@gmail.com',
                link: 'mailto:yudhafebrirastuama@gmail.com'
            },
            {
                icon: ['fab', 'instagram'],
                contact: 'yudfrs18',
                link: 'https://instagram.com/yudfrs18'
            },
            {
                icon: ['fab', 'twitter'],
                contact: 'yudfrs18',
                link: 'https://twitter.com/yudfrs18'
            },
            {
                icon: 'mobile-alt',
                contact: '082242361964',
            },
            {
                icon: ['fab', 'whatsapp'],
                contact: '082242361964',
                link: 'https://wa.me/6282242361964'
            },
            {
                icon: ['fab', 'telegram'],
                contact: '082242361964 / yufebra',
                link: 'https://t.me/yufebra'
            },
            {
                icon: ['fab', 'linkedin'],
                contact: 'Yudha Febri Rastuama',
                link: 'https://linkedin.com/in/yudhafebrirastuama'
            },
        ]

        const cards = [];
        contacts.forEach((item, iter) => {
            cards.push(
                (
                    <a href={item.link} key={iter} className="bg-white rounded-lg p-2 hover:bg-green-300 focus:bg-blue-500 focus:text-white text-sm" rel="noreferrer" target="_blank">
                        <FontAwesomeIcon icon={ item.icon } className="mr-1" size="lg" />{ item.contact }
                    </a>
                )
            )
        })

        return (
            <div className="bg-black p-10 flex justify-center" id="contact">
                <div className="w-full max-w-5xl p-2">
                    <Title color="white">Find Me</Title>
                    <div className="flex flex-wrap gap-2">
                        { cards }
                    </div>
                </div>
            </div>
        )
    }
}

export default MyContact;
