import React from "react";
import {FontAwesomeIcon} from "@fortawesome/react-fontawesome";
import image from '../../assets/img/me-transparent.png';

class TopContentWelcome extends React.Component{
    render() {
        return (
            <div className="center-screen bg-green-400">
                <div className="stretch-row flex-col md:flex-row items-center">
                    <div className="w-full max-w-5xl p-2">
                        <p className="text-xs">Hello, my name is...</p>
                        <p className="text-4xl font-extrabold">Yudha Febri Rastuama</p>
                    </div>
                    <img className="h-96" src={image} alt="me" />
                </div>
                <a className="absolute bottom-10 text-center animate-bounce" href="#about">
                    <p className="text-sm mb-2">Continue Below</p>
                    <FontAwesomeIcon icon="angle-down" size="lg"/>
                </a>
            </div>
        )
    }
}

export default TopContentWelcome;
