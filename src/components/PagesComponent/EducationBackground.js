import React from "react";
import BasicCard from "../Cards/BasicCard";

class EducationBackground extends React.Component{
    render() {
        return(
            <BasicCard>
                <h1 className="font-extrabold text-lg">Politeknik Elektronika Negeri Surabaya</h1>
                <p className="text-xs">D3 Teknik Informatika | 2018-2021</p>
            </BasicCard>
        )
    }
}

export default EducationBackground;
